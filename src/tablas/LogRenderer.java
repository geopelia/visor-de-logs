package tablas;

import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Color;
import java.awt.Font;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogRenderer extends DefaultTableCellRenderer {
    Pattern patternDebug, patternError, patternWarn;
    Color redo, aoi, genki;

    public LogRenderer() {
        super();
        patternDebug = Pattern.compile("\\[DEBUG\\]");
        patternError = Pattern.compile("^(([A-Za-z]{2,}\\.)|\\[ERROR\\]|Caused|\\sat)");
        patternWarn = Pattern.compile("\\[\\sWARN\\]");
        redo = new Color(163, 54, 67);
        aoi = new Color(41, 38, 47);
        genki = new Color(195, 186, 165);


    }

    @Override
    protected void setValue(Object value) {
        revalidate();
        String text = value.toString();
        setText(text);
        setBackground(Color.white);
        setForeground(Color.darkGray);
        Matcher matcher = patternDebug.matcher(text);
        if (matcher.find()) {
            setForeground(aoi);
            return;
        }
        matcher = patternWarn.matcher(text);
        if (matcher.find()) {
            setBackground(genki);
            setForeground(Color.white);
            return;
        }
        matcher = patternError.matcher(text);
        if (matcher.find()) {
            setFont(new Font("Tahoma", Font.BOLD, 12));
            setForeground(redo);
        }
        setAutoscrolls(true);
    }
}

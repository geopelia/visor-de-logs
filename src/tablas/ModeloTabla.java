package tablas;

import archivos.FileManager;
import archivos.Files;

import javax.swing.table.AbstractTableModel;
import java.io.FileNotFoundException;
import java.util.Vector;

public class ModeloTabla extends AbstractTableModel {
    private Vector<String> columnNames = new Vector<String>(1);
    private FileManager manager;
    private Vector<Vector> rowData;
    private Files files;

    public ModeloTabla(Files file) {
        manager = new FileManager();
        this.columnNames.add("Log");
        readFile(file);
        this.files = file;
    }

    private void readFile(Files file) {
        try {
            this.rowData = manager.readFile(file.getPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            this.rowData = new Vector<Vector>(1);
        }
    }

    @Override
    public int getRowCount() {
        return rowData.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rowData.get(rowIndex).get(columnIndex);
    }

    public String getColumnName(int col) {
        return columnNames.get(col);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        readFile(files);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }
}

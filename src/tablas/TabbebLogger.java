package tablas;

import archivos.CustomFileListener;
import archivos.Files;
import archivos.VisorArchivos;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class TabbebLogger {
    private JTabbedPane tabbedPane1;
    private JTable oraTrunkTable;
    private JTable wesTrunkTable;
    private JTable ora131Table;
    private JPanel mainPanel;
    private JPanel log1;
    private JPanel log2;
    private JPanel log3;
    private JScrollPane scroll1;
    private JScrollPane scroll2;
    private JScrollPane scroll3;

    public TabbebLogger() {


        oraTrunkTable.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    sendTableToLastRow(oraTrunkTable);
                super.keyReleased(e);
            }
        });
        tabbedPane1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    selectTable(tabbedPane1.getSelectedIndex());
                super.keyPressed(e);
            }
        });
        wesTrunkTable.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    sendTableToLastRow(wesTrunkTable);
                super.keyReleased(e);
            }
        });
        ora131Table.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    sendTableToLastRow(ora131Table);
                super.keyReleased(e);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("TabbebLogger");
        frame.setContentPane(new TabbebLogger().mainPanel);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.requestFocus();
    }

    private void selectTable(int selectedIndex) {
        switch (selectedIndex) {
            case 0:
                sendTableToLastRow(oraTrunkTable);
                break;
            case 1:
                sendTableToLastRow(wesTrunkTable);
                break;
            case 2:
                sendTableToLastRow(ora131Table);
                break;
        }
    }

    public void fireUpdateTable(int selectedIndex) {
        switch (selectedIndex) {
            case 0:
                updateTable(oraTrunkTable);
                break;
            case 1:
                updateTable(wesTrunkTable);
                break;
            case 2:
                updateTable(ora131Table);
                break;
        }

    }

    private void updateTable(JTable table) {
        table.setValueAt("-", 0, 0);
        table.updateUI();
        sendTableToLastRow(table);
    }

    private void sendTableToLastRow(JTable table) {
        int bottomRow = table.getRowCount() - 1;
        Rectangle rect = table.getCellRect(bottomRow, 0, true);
        table.scrollRectToVisible(rect);
    }

    private void createUIComponents() throws FileNotFoundException {
        oraTrunkTable = new JTable(new ModeloTabla(Files.WEBLOGIC_TRUNK_PATH));
        oraTrunkTable.setDefaultRenderer(Object.class, new LogRenderer());
        ora131Table = new JTable(new ModeloTabla(Files.WEBLOGIC_131_PATH));
        ora131Table.setDefaultRenderer(Object.class, new LogRenderer());
        wesTrunkTable = new JTable(new ModeloTabla(Files.WEBSPHERE_TRUNK_PATH));
        wesTrunkTable.setDefaultRenderer(Object.class, new LogRenderer());
        resizeColumnWidth(oraTrunkTable);
        resizeColumnWidth(ora131Table);
        resizeColumnWidth(wesTrunkTable);
        CustomFileListener kaso = new CustomFileListener(this);
        new VisorArchivos(kaso);
    }

    public void resizeColumnWidth(JTable table) {
        TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 50; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width, width);
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }
}

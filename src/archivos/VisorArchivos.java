package archivos;

import org.apache.commons.vfs.FileSystemException;
import org.apache.commons.vfs.FileSystemManager;
import org.apache.commons.vfs.VFS;
import org.apache.commons.vfs.impl.DefaultFileMonitor;

import java.io.IOException;
import java.util.Properties;

public class VisorArchivos {
    public VisorArchivos(CustomFileListener listener) {
        try {
            Properties properties = new Properties();
            properties.load(getClass().getResourceAsStream("/configuracion.properties"));
            FileSystemManager fsManager = VFS.getManager();
            DefaultFileMonitor fm = new DefaultFileMonitor(listener);
            for (Files files : Files.values()) {
                fm.addFile(fsManager.resolveFile(files.getPath()));
            }
            Long delay = 5000L;
            if (properties.getProperty("delay") != null ||
                    !properties.getProperty("delay").isEmpty()) {
                delay = Long.parseLong(properties.getProperty("delay"));
            }
            fm.setDelay(delay);
            fm.start();
        } catch (FileSystemException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

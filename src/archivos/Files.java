package archivos;

import java.io.IOException;
import java.util.Properties;

public enum Files {
    WEBLOGIC_TRUNK_PATH(0, "ruta_oratrunk"),
    WEBSPHERE_TRUNK_PATH(1, "ruta_westrunk"),
    WEBLOGIC_131_PATH(2, "ruta_other");


    private final int option;
    private final String path;

    Files(int i, String s) {
        this.option = i;
        this.path = s;
    }

    public static Files getFiles(String s) {
        for (Files file : Files.values()) {
            if (s.contains(file.getPath())) return file;
        }
        return null;
    }

    public int getOption() {
        return option;
    }

    public String getPath() {
        Properties properties = new Properties();
        try {
            properties.load(getClass().getResourceAsStream("/configuracion.properties"));
            return properties.getProperty(path);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}

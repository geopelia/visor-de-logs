package archivos;

import org.apache.commons.vfs.FileChangeEvent;
import org.apache.commons.vfs.FileListener;
import tablas.TabbebLogger;

public class CustomFileListener implements FileListener {
    private TabbebLogger view;

    public CustomFileListener(TabbebLogger view) {
        this.view = view;
    }

    @Override
    public void fileCreated(FileChangeEvent fileChangeEvent) throws Exception {

    }

    @Override
    public void fileDeleted(FileChangeEvent fileChangeEvent) throws Exception {

    }

    @Override
    public void fileChanged(FileChangeEvent fileChangeEvent) throws Exception {
        String filename = fileChangeEvent.getFile().getName().getFriendlyURI().replace("/", "\\");
        try {
            int opt = Files.getFiles(filename).getOption();
            view.fireUpdateTable(opt);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }
}

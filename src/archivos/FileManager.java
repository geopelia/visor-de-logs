package archivos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Vector;

public class FileManager {

    public Vector readFile(String fileName) throws FileNotFoundException {
        Vector<Vector> result = new Vector<Vector>();
        Vector<String> buffer;
        String NL = System.getProperty("line.separator");
        Scanner scanner = new Scanner(new FileInputStream(fileName));
        try {
            while (scanner.hasNextLine()) {
                buffer = new Vector<String>(1);
                buffer.add(scanner.nextLine() + NL);
                result.add(buffer);
            }
        } finally {
            scanner.close();
        }
        return result;
    }
}

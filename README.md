# README #
Este es un visor de archivos sencillo con múltiples pestañas hecho usando componentes swing. Es un trabajo sencillo donde no se siguen las convenciones del lenguaje. 

### Dependencias ###
Se necesitan las bibliotecas:
* commons-vfs
* commons-logging


### Pasos previos ###
* Dentro de la carpeta src se debe crear un archivo llamado configuracion.properties. Dentro se deben definir las siguientes propiedades.
ruta_westrunk=
ruta_oratrunk=
ruta_other=
delay=
Las 3 primeras son las rutas donde absolutas de los archivos log a mostrar y la ultima cuanto tiempo en milisegundos debe pasar para actualizar el visor. Por lo menos se deben configurar las 3 primeras propiedades.